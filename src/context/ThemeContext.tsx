import {createContext} from 'react'
import {TDesignMode, TShowcaseType} from '../store/types'

interface IThemeContext {
    dark: boolean
    toggleDark: () => void
    designMode: TDesignMode | null,
    changeDesignMode: (val: TDesignMode | null) => void
    showcaseType: TShowcaseType | null
    changeShowcaseType: (val: TShowcaseType | null) => void
}

const initialState: IThemeContext = {
    dark: true,
    toggleDark: () => {},
    designMode: null,
    changeDesignMode: () => {},
    showcaseType: null,
    changeShowcaseType: () => {},
}

const ThemeContext = createContext<IThemeContext>(initialState)

export default ThemeContext
