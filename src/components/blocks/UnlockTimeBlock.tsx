import React, {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {Trans, useTranslation} from 'react-i18next'
import {AppDispatch} from '../../store/store'
import {getUnlockTime, setUnlockTime, setUnlockTimeError} from '../../store/inputSlice'
import {InputElement} from '../elements'
import {MAX_UNLOCK_TIME} from '../../utils/constants'

const UnlockTimeBlock = () => {
    const {t} = useTranslation()
    const unlockTime = useSelector(getUnlockTime)

    const dispatch = useDispatch<AppDispatch>()

    useEffect(() => {
        return () => {
            dispatch(setUnlockTime(null))
        }
    }, [])

    const onChangeHandler = (value: string) => {
        if (value === '') {
            dispatch(setUnlockTime(null))
            return
        }

        let time = parseInt(value)
        if (isNaN(time)) {
            dispatch(setUnlockTimeError(t('error.mustEnter', {name: t('form.label.unlockTime')})))
            return
        }

        time = time < 0 ? 0 : time
        time = time > MAX_UNLOCK_TIME ? MAX_UNLOCK_TIME : time
        dispatch(setUnlockTime(time))
        dispatch(setUnlockTimeError(null))
    }

    return <div className="mb-7">
        <h2 className="mb-4">{t('form.label.unlockTime')}</h2>
        <div className="row">
            <div className="col-6 col-sm-4 col-lg-2">
                <InputElement
                    error={unlockTime.error.status}
                    errorText={unlockTime.error.text}
                    label={<Trans i18nKey={'form.label.maxDays'} components={[<span className="text-muted"/>]}/>}
                    value={unlockTime.value !== null ? unlockTime.value.toString() : ''}
                    onChange={onChangeHandler}
                />
            </div>
        </div>
    </div>
}

export default UnlockTimeBlock
