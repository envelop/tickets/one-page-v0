import React, {useContext, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {useConnectWallet} from '@web3-onboard/react'
import {useTranslation} from 'react-i18next'
import {getInitializeError, getWalletAddress, setWalletAddress, walletChanged} from '../../store/appSlice'
import {ThemeContext} from '../../context'
import {getFromStorage} from '../../store/storage'
import {AppDispatch} from '../../store/store'

const Initialize = () => {
    const {t} = useTranslation()
    const {dark} = useContext(ThemeContext)
    const error = useSelector(getInitializeError)
    const walletAddress = useSelector(getWalletAddress)

    const dispatch = useDispatch<AppDispatch>()
    const [{wallet}, connect] = useConnectWallet()

    useEffect(() => {
        const connectedWallet = getFromStorage('connectedWallet')
        if (connectedWallet) {
            connect({autoSelect: {label: connectedWallet, disableModals: true}})
        }
    }, [])
    useEffect(() => {
        if (wallet && walletAddress !== wallet.accounts[0].address) {
            const walletInStorage = getFromStorage('walletAddress')
            dispatch(setWalletAddress(wallet.accounts[0].address))
            if (!!walletInStorage && walletInStorage !== wallet.accounts[0].address) {
                dispatch(walletChanged())
            }
        }
    }, [wallet])

    return <div className="wrapper" data-theme={dark ? '' : 'light'}>
        <main className="s-main page-loading">
            <div className="block-loading">
                <img className="img-loading" src="/static/img/logo-anim.svg" alt=""/>
                <div className={`mt-5 ${error === '' ? '' : 'text-danger'}`}>
                    {error === '' ? `${t('status.initializing')}...` : error}
                </div>
            </div>
        </main>
    </div>
}

export default Initialize
