import React, {useContext, useEffect} from 'react'
import {Outlet} from 'react-router-dom'
import {ThemeContext} from '../../context'

const LayoutClassic = () => {
    const {changeDesignMode, changeShowcaseType} = useContext(ThemeContext)

    useEffect(() => {
        changeShowcaseType('classic')
        changeDesignMode('classic-mode')
        return () => {
            changeShowcaseType(null)
            changeDesignMode(null)
        }
    }, [])

    return <Outlet/>
}

export default LayoutClassic
