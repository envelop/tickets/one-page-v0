import React, {useRef, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {ethers} from 'ethers'
import {useTranslation} from 'react-i18next'
import {
    getCurrentNetwork,
    getModalEditTicketPrices,
    setModalEditTicketPrices,
} from '../../store/appSlice'
import {ButtonElement, DropdownSelector, InputElement} from '../elements'
import {getCoinsObject, getStandardCoinsList, requestCoin} from '../../store/coinsSlice'
import {IPrice, ITicketPrice} from '../../store/types'
import {CHAINS, CUSTOM_DROPDOWN_ITEM, ERC20_TOKENS, NULL_ADDRESS} from '../../utils/constants'
import {checkPriceFormat} from '../../utils/functions'
import {AppDispatch} from '../../store/store'
import {AddIcon, CloseIcon} from '../icons'

interface propsType {
    show: boolean
}

const getEmptyPrice = (): ITicketPrice => ({
    price: '',
    priceError: '',
    decimals: 18,
    token: NULL_ADDRESS,
    customContract: '',
    customContractError: ''
})
const isIPrice = (variable: IPrice[] | ITicketPrice[]): variable is IPrice[] => {
    return typeof (variable as IPrice[])[0]?.payWith === 'string'
}

const ModalEditTicketPrices = (props: propsType) => {
    const {t} = useTranslation()
    const modalFade = useRef(null)
    const coinsList = useSelector(getStandardCoinsList)
    const coinsObject = useSelector(getCoinsObject)
    const currentNetwork = useSelector(getCurrentNetwork)
    const getInitPrices = (value: ITicketPrice[] | IPrice[]): ITicketPrice[] => {
        if (!value) {
            return []
        }

        if (!isIPrice(value)) {
            return value
        }

        let result: ITicketPrice[] = []
        for (let item of value) {
            const decimals = coinsObject[item.payWith].decimals
            const customContract = ERC20_TOKENS[currentNetwork || ''].indexOf(item.payWith) < 0 && item.payWith !== NULL_ADDRESS ? item.payWith : ''
            result.push({
                customContract,
                customContractError: '',
                decimals,
                price: ethers.utils.formatUnits(item.amount, coinsObject[item.payWith].decimals),
                priceError: '',
                token: customContract === '' ? item.payWith : 'custom',
            })
        }
        return result
    }
    const modalEditTicketPrices = useSelector(getModalEditTicketPrices)
    const [prices, setPrices] = useState<ITicketPrice[]>(getInitPrices(modalEditTicketPrices?.prices || []))
    const setPrice = (index: number, value: ITicketPrice) => {
        setPrices((prevState) => {
            let newState = [...prevState]
            newState[index] = {...value}
            return newState
        })
    }

    const dispatch = useDispatch<AppDispatch>()

    const closeModal = () => {
        dispatch(setModalEditTicketPrices(null))
    }
    const customContractHandler = (index: number) => (value: string) => {
        if (ethers.utils.isAddress(value) || value === '') {
            setPrice(index, {...prices[index], customContract: value.toLowerCase(), customContractError: ''})
            dispatch(requestCoin({address: value, network: currentNetwork}))
        } else {
            setPrice(index, {
                ...prices[index],
                customContractError: t('error.customContractError', {name: currentNetwork ? CHAINS[currentNetwork].eipPrefix : 'ERC'})
            })
        }
    }
    const onChangePriceHandler = (index: number) => (value: string) => {
        const price = value.replace(',', '.')
        if (price === '' || checkPriceFormat(price, prices[index].decimals)) {
            setPrice(index, {...prices[index], price})
        }
    }
    const onChangeTokenHandler = (index: number, id: string) => {
        setPrice(index, {...prices[index], token: id, decimals: coinsObject[id]?.decimals || 18})
    }
    const setHandler = (): void => {
        let error = false
        let newPrices = [...prices]
        for (let i = 0; i < prices.length; i++) {
            const item = prices[i]
            newPrices[i] = {...item}
            const price = Number(item.price)
            if (isNaN(price) || price < 0) {
                error = true
                newPrices[i].priceError = t('error.mustBeGreater', {name: t('form.label.price'), value: 0})
            } else {
                newPrices[i].priceError = ''
            }

            if (item.token === CUSTOM_DROPDOWN_ITEM.id && !ethers.utils.isAddress(item.customContract)) {
                error = true
                newPrices[i].customContractError = t('error.wrong', {name: t('form.label.address')})
            } else {
                newPrices[i].customContractError = ''
            }
        }
        setPrices(newPrices)

        if (error) {
            console.log(error)
            return
        }

        modalEditTicketPrices?.setPrices(newPrices)
        closeModal()
    }

    if (!props.show) {
        return null
    }

    return <>
        <div
            className={`modal modal-lg fade show`}
            ref={modalFade}
            tabIndex={-1}
            aria-labelledby="Edit price for the selected ticket"
            aria-hidden="true"
            style={{display: 'block'}}
            onMouseDown={(e) => {
                if (e.target === modalFade.current) {
                    closeModal()
                }
            }}
        >
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h2 className="modal-title">{t('modal.title.editPriceForTicket')}</h2>
                        <button className="btn p-2 btn-with-opacity btn-modal-close" onClick={closeModal}>
                            <CloseIcon/>
                        </button>
                    </div>
                    <div className="modal-body pt-0">
                        <div className="mb-lg-2">
{/*
                            {pricesError !== '' ? <div className={'alert-danger'}>{pricesError}</div> : null}
*/}
                            <div className="row gx-3 d-none d-lg-flex">
                                <div className="col-lg-3">
                                    <div className="mb-2">
                                        <label className="form-label">{t('form.label.price')}</label>
                                    </div>
                                </div>
                                <div className="col-lg-3">
                                    <div className="mb-2">
                                        <label className="form-label">{t('form.label.token')}</label>
                                    </div>
                                </div>
                            </div>
                            {prices.map((item, index) => {
                                return <div key={index} className="row gx-2 gx-lg-3">
                                    <div className="col-6 col-lg-3">
                                        <div className="mb-lg-3">
                                            <label className="form-label d-lg-none">{t('form.label.price')}</label>
                                            <InputElement
                                                errorText={item.priceError}
                                                value={item.price.toString()}
                                                onChange={onChangePriceHandler(index)}
                                                placeholder={'0'}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-6 col-lg-3">
                                        <div className="mb-lg-3">
                                            <label className="form-label d-lg-none">{t('form.label.token')}</label>
                                            <DropdownSelector
                                                currentItem={item.token}
                                                list={coinsList}
                                                setItem={(id) => {
                                                    onChangeTokenHandler(index, id)
                                                }}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-12 col-lg">
                                        {item.token === CUSTOM_DROPDOWN_ITEM.id ?
                                            <div className="mt-2 mt-lg-0 mb-lg-3">
                                                <InputElement
                                                    errorText={item.customContractError}
                                                    value={item.customContract}
                                                    onChange={customContractHandler(index)}
                                                    placeholder={t('form.label.tokenAddress')}
                                                />
                                            </div>
                                            :
                                            null
                                        }
                                    </div>
                                    <div className="col-auto col-lg-2">
                                        <div className="mb-2 mb-lg-3">
                                            <ButtonElement
                                                link
                                                className={'link-danger w-100 px-0'}
                                                onClick={() => {
                                                    setPrices((prevState) => {
                                                        let newState = []
                                                        for (let i = 0; i < prevState.length; i++) {
                                                            if (i !== index) {
                                                                newState.push(prevState[i])
                                                            }
                                                        }
                                                        return newState
                                                    })
                                                }}
                                            >{t('button.delete')}</ButtonElement>
                                        </div>
                                    </div>
                                </div>
                            })}
                        </div>
                        <div className="py-2 pb-lg-0">
                            <ButtonElement
                                small
                                outline
                                onClick={() => {
                                    setPrices((prevState) => {
                                        return [...prevState, getEmptyPrice()]
                                    })
                                }}
                            >
                                <AddIcon/>
                                <span>{t('button.addMore')}</span>
                            </ButtonElement>
                        </div>
                    </div>
                    <div className="modal-footer justify-content-between">
{/*
                        <div className="mb-3 mb-lg-0 pe-4">
                            <div className="mb-1">
                                <span className="text-muted">Network </span>
                                <span>{CHAINS[currentNetwork].label}</span>
                            </div>
                            <div className="mb-1">
                                <span className="text-muted">Showcase </span>
                                <span>{displayName}</span>
                            </div>
                        </div>
*/}
                        <ButtonElement className={'flex-grow-1 flex-lg-grow-0'} onClick={setHandler}>
                            {t('button.savePrices')}
                        </ButtonElement>
                    </div>
                </div>
            </div>
        </div>
    </>
}

export default ModalEditTicketPrices
