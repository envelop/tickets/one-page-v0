import React, {useRef} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {useTranslation} from 'react-i18next'
import {getModalAddEventToShowcase, setModalAddEventToShowcase} from '../../store/appSlice'
import {ButtonElement} from '../elements'
import {AppDispatch} from '../../store/store'
import {CloseIcon} from '../icons'
import {SelectEvent, SelectOrganizer, SelectTicketLevel} from '../profile'
import {addEventToShowcase, getSelectedEventId} from '../../store/eventsSlice'
import {getSelectedTicketLevelName} from '../../store/ticketsSlice'

interface propsType {
    show: boolean
}

const ModalAddEventToShowcase = (props: propsType) => {
    const {t} = useTranslation()
    const modalFade = useRef(null)
    const eventId = useSelector(getSelectedEventId)
    const modal = useSelector(getModalAddEventToShowcase)
    const ticketLevelName = useSelector(getSelectedTicketLevelName)

    const dispatch = useDispatch<AppDispatch>()

    const closeModal = () => {
        dispatch(setModalAddEventToShowcase(null))
    }
    const editHandler = () => {
        if (!modal || ticketLevelName === '') {
            return
        }

        let error = false
        if (!eventId) {
            error = true
        }

        if (error) {
            return
        }
        dispatch(addEventToShowcase(ticketLevelName))
    }

    if (!modal) {
        closeModal()
    }

    if (!props.show) {
        return null
    }

    return <div
        className={`modal modal-lg fade show`}
        ref={modalFade}
        tabIndex={-1}
        aria-labelledby="Edit a showcase"
        aria-hidden="true"
        style={{display: 'block'}}
        onMouseDown={(e) => {
            if (e.target === modalFade.current) {
                closeModal()
            }
        }}
    >
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <h2 className="modal-title">{t('modal.title.editShowcaseEvent')}</h2>
                    <button className="btn p-2 btn-with-opacity btn-modal-close" onClick={closeModal}>
                        <CloseIcon/>
                    </button>
                </div>
                <div className="modal-body pt-0">
                    <div className="row">
                        <div className="col-6">
                            <div className="mb-4">
                                <SelectOrganizer/>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="mb-4">
                                <SelectEvent/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="mb-4">
                                <SelectTicketLevel/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer justify-content-end">
                    <ButtonElement
                        className={'flex-grow-1 flex-lg-grow-0'}
                        onClick={editHandler}
                        disabled={!eventId}
                    >{t('button.link')}</ButtonElement>
                </div>
            </div>
        </div>
    </div>
}

export default ModalAddEventToShowcase
