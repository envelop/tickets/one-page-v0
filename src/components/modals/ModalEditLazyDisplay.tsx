import React, {useRef, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {useTranslation} from 'react-i18next'
import {getModalEditLazyDisplay, getWalletAddress, setModalEditLazyDisplay} from '../../store/appSlice'
import {ButtonElement, DateElement, InputElement} from '../elements'
import {AppDispatch} from '../../store/store'
import {CloseIcon} from '../icons'
import {getShowcase, setDisplayParams} from '../../store/lazyMintingSlice'

interface propsType {
    show: boolean
}

const ModalEditLazyDisplay = (props: propsType) => {
    const {t} = useTranslation()
    const modalFade = useRef(null)
    const showcase = useSelector(getShowcase)
    const [disableDate, setDisableDate] = useState<Date | null>(showcase ? new Date(showcase.disableAfter * 1000) : null)
    const [disableError, setDisableError] = useState('')
    const [enableDate, setEnableDate] = useState<Date | null>(showcase ? new Date(showcase.enableAfter * 1000) : null)
    const [enableError, setEnableError] = useState('')
    const modalEditLazyDisplay = useSelector(getModalEditLazyDisplay)
    const walletAddress = useSelector(getWalletAddress)

    const dispatch = useDispatch<AppDispatch>()

    const closeModal = () => {
        dispatch(setModalEditLazyDisplay(null))
    }
    const editDisplay = () => {
        if (!modalEditLazyDisplay) {
            return
        }

        let error = false
        const enableTimestamp = enableDate ? Math.floor(enableDate.getTime() / 1000) : 0
        const disableTimestamp = disableDate ? Math.floor(disableDate.getTime() / 1000) : 0

        if (enableTimestamp <= 0) {
            error = true
            setEnableError(t('error.wrong', {name: t('form.label.enableDate')}))
        } else {
            setEnableError('')
        }
        if (disableTimestamp <= 0) {
            error = true
            setDisableError(t('error.wrong', {name: t('form.label.disableDate')}))
        } else if (disableTimestamp <= enableTimestamp) {
            error = true
            setDisableError(t('error.mustBeGreater', {name: t('form.label.disableDate'), value: t('form.label.enableDate')}))
        } else {
            setDisableError('')
        }

        if (error || !walletAddress) {
            return
        }
        dispatch(setDisplayParams({
            displayName: modalEditLazyDisplay,
            beneficiary: walletAddress,
            enableAfter: enableTimestamp,
            disableAfter: disableTimestamp,
            edit: true,
        }))
    }

    if (!modalEditLazyDisplay || modalEditLazyDisplay === '') {
        closeModal()
    }

    if (!props.show) {
        return null
    }

    return <div
        className={`modal modal-lg fade show`}
        ref={modalFade}
        tabIndex={-1}
        aria-labelledby="Edit a showcase"
        aria-hidden="true"
        style={{display: 'block'}}
        onMouseDown={(e) => {
            if (e.target === modalFade.current) {
                closeModal()
            }
        }}
    >
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <h2 className="modal-title">{t('modal.title.editSmartShowcase')}</h2>
                    <button className="btn p-2 btn-with-opacity btn-modal-close" onClick={closeModal}>
                        <CloseIcon/>
                    </button>
                </div>
                <div className="modal-body pt-0">
                    <div className="row">
                        <div className="col-12">
                            <div className="mb-4">
                                <InputElement
                                    value={modalEditLazyDisplay || ''}
                                    onChange={() => {}}
                                    readonly={true}
                                />
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-4">
                                <DateElement
                                    value={enableDate}
                                    onChange={setEnableDate}
                                    label={t('form.label.enableDate')}
                                    errorText={enableError}
                                />
                            </div>
                        </div>
                        <div className="col-12 col-sm-6">
                            <div className="mb-4">
                                <DateElement
                                    value={disableDate}
                                    onChange={setDisableDate}
                                    label={t('form.label.disableDate')}
                                    errorText={disableError}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer justify-content-end">
                    <ButtonElement
                        className={'flex-grow-1 flex-lg-grow-0'}
                        onClick={editDisplay}
                    >{t('button.save')}</ButtonElement>
                </div>
            </div>
        </div>
    </div>
}

export default ModalEditLazyDisplay
