import React, {useContext, useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {useSearchParams} from 'react-router-dom'
import {Trans, useTranslation} from 'react-i18next'
import {ethers} from 'ethers'
import {useConnectWallet} from '@web3-onboard/react'
import {AlertElement, ButtonElement, VideoButton} from '../elements'
import {ThemeContext} from '../../context'
import {SelectGiftsCollection} from './index'
import {CollateralBlock, PropertiesBlock, RecipientsBlock, SelectImageBlock, TitleAndDescriptionBlock} from '../blocks'
import {AppDispatch} from '../../store/store'
import {mintGifts} from '../../store/giftsSlice'
import {
    getCollaterals,
    getDescription, getImageUrl,
    getRecipients,
    getTitle, setCollateralsError, setDescription,
    setDescriptionError, setExternalUrl, setImageUrlError, setRecipientsError, setTitle,
    setTitleError
} from '../../store/inputSlice'
import {
    checkWallet,
    getCurrentNetwork,
    getSelectedGiftCollection, getSelectedGiftCollectionError, getWalletAddress,
    sendRequestWithAuth,
    setModalGenerateAIGift, setSelectedGiftCollectionError
} from '../../store/appSlice'
import {getBalances} from '../../store/userSlice'
import {APP_URL, CHAINS, CUSTOM_DROPDOWN_ITEM, ENVIRONMENT, NULL_ADDRESS} from '../../utils/constants'
import {getPublicCollectionDropdown} from '../../utils/functions'
import {GATracker} from '../GATracker'

const Gifts = () => {
    const {t} = useTranslation()
    const {dark} = useContext(ThemeContext)
    const [searchParams] = useSearchParams('')
    const [activeTab, setActiveTab] = useState<number>(1)
    const balances = useSelector(getBalances)
    const collaterals = useSelector(getCollaterals)
    const currentNetwork = useSelector(getCurrentNetwork)
    const description = useSelector(getDescription)
    const imageUrl = useSelector(getImageUrl)
    const recipients = useSelector(getRecipients)
    const selectedCollection = useSelector(getSelectedGiftCollection)
    const selectedCollectionError = useSelector(getSelectedGiftCollectionError)
    const title = useSelector(getTitle)
    const walletAddress = useSelector(getWalletAddress)
    const publicDropdown = getPublicCollectionDropdown()

    const dispatch = useDispatch<AppDispatch>()
    const [{wallet}] = useConnectWallet()

    useEffect(() => {
        GATracker.sendPageTracker('/gifts', 'View gifts page')
        if (searchParams.has('tab') && searchParams.get('tab') !== '' && Number(searchParams.get('tab')) === 2) {
            setTemplateActiveTab(false)
        }
    }, [])
    useEffect(() => {
        checkBalance()
    }, [collaterals, recipients])

    const checkBalance = (): boolean => {
        const batch = recipients.value.length
        for (let item of collaterals.value) {
            if (item.price === '') {
                if (collaterals.error.text !== t('error.setPrice')) {
                    dispatch(setCollateralsError(t('error.setPrice')))
                }
                return false
            }

            const token = item.token === CUSTOM_DROPDOWN_ITEM.id ? item.customContract : item.token
            let balance: string | undefined = balances[token]?.balance
            if (token === NULL_ADDRESS && wallet && currentNetwork) {
                balance = wallet.accounts[0]?.balance?.[CHAINS[currentNetwork].token]
            }
            if (balance === undefined ||
                ethers.utils.parseEther(balance).lt(ethers.utils.parseEther(item.price).mul(batch || 1))
            ) {
                if (collaterals.error.text !== t('error.insufficientBalance')) {
                    dispatch(setCollateralsError(t('error.insufficientBalance')))
                }
                return false
            }
        }
        if (collaterals.error.status) {
            dispatch(setCollateralsError(null))
        }
        return true
    }
    const checkButtonDisabled = (): boolean => {
        if (!selectedCollection && !selectedCollectionError) {
            //todo: Warning: Cannot update a component
            dispatch(setSelectedGiftCollectionError(t('error.collectionNotSelected')))
        }
        return recipients.error.status || collaterals.error.status || imageUrl.error.status || title.error.status ||
            description.error.status || !selectedCollection
    }
    const createGifts = () => {
        let error = false
        if (!selectedCollection) {
            error = true
            dispatch(setSelectedGiftCollectionError(t('error.collectionNotSelected')))
        }
        const batch = recipients.value.length
        if (!batch) {
            error = true
            dispatch(setRecipientsError(t('error.mustBeGreater', {name: t('form.label.recipients'), value: 0})))
        } else {
            if (!checkBalance()) {
                error = true
            }
        }
        if (imageUrl.value === null) {
            error = true
            dispatch(setImageUrlError(t('error.chooseImage')))
        }
        if (title.value === '') {
            error = true
            dispatch(setTitleError(t('error.mustEnter', {name: t('form.label.giftName')})))
        } else if (title.error.status) {
            error = true
        }
        if (description.value === '') {
            error = true
            dispatch(setDescriptionError(t('error.mustEnter', {name: t('form.label.description')})))
        } else if (description.error.status) {
            error = true
        }
        if (error) {
            return
        }

        GATracker.sendEventTracker('Gifts', 'MintGifts', walletAddress ? 'Authorized' : 'Not authorized')
        dispatch(checkWallet({request: sendRequestWithAuth(mintGifts())}))
    }
    const setAIActiveTab = () => {
        GATracker.sendEventTracker('Gifts', 'ClickAIButton', walletAddress ? 'Authorized' : 'Not authorized')
        setActiveTab(1)
        dispatch(setModalGenerateAIGift({template: false}))
    }
    const setTemplateActiveTab = (sendEvent: boolean = true) => {
        if (sendEvent) {
            GATracker.sendEventTracker('Gifts', 'ClickDailyTemplate', walletAddress ? 'Authorized' : 'Not authorized')
        }
        setActiveTab(2)
        //dispatch(setModalGenerateAIGift({template: true}))
        dispatch(setImageUrlError(null))
        dispatch(setExternalUrl(`${APP_URL}/static/img/templateTab.png`))
        dispatch(setTitle('Winter Wonderland Gift'))
        dispatch(setTitleError(null))
        dispatch(setDescription('A magical winter landscape adorned with glistening snowflakes and twinkling lights. This image captures the warmth and joy of the holiday season, featuring a cozy cabin tucked beneath frosty trees, with a cheerful snowman and festive decorations. It\'s the perfect representation of celebration, togetherness, and the spirit of giving—a heartfelt gift for loved ones.'))
        dispatch(setDescriptionError(null))
    }

    return <div className="page-content page--mint-gifts">
        <img className="page-bg" src={`/static/img/bg/${dark ? 'dark' : 'light'}-mode/bg-gifts.svg`} alt=""/>
        <div className="container">
            <div className="row mb-2">
                <div className="col">
                    <h1>{t('section.gifts')}</h1>
                </div>
                <div className="col-auto">
                    <VideoButton
                        link={'https://www.youtube.com/embed/HPcWJZa0wEI?si=s-VzunKIHmsa67QQ'}
                        title={'How to give an on-chain gift?'}
                    />
                </div>
            </div>
            <div className="row mb-4">
                <div className="col-lg-8">
                    <p>{t('gifts.pageDescription')}</p>
                    <p>
                        <a
                            href={'https://docs.myshch.io/myshch/tutorials/for-event-organisers/gifts-for-visitors'}
                            target={'_blank'}
                            rel={'noreferrer'}
                        >{t('action.readMore')}</a>
                    </p>
                    <p>
                        <a
                            href={'https://owlto.finance/?ref=0x23802E21c6cd72C091792BfB9f7afC2265cc68d6'}
                            target={'_blank'}
                            rel={'noreferrer'}
                        >{t('button.bridgeAndSwap')}</a>
                    </p>
                </div>
            </div>
            <SelectGiftsCollection/>
            <div className="mb-6 pb-4">
                <h2 className="mb-6">{t('header.imageForGift')}</h2>
                <div className="row mb-4">
                    <div className="mb-2 col-md-6 col-lg-4 col-xl-3">
                        <button
                            className={`btn btn__img-source ${activeTab === 0 ? 'active' : ''}`}
                            onClick={() => setActiveTab(0)}
                        >
                            <span className="desc">
                                <span className="title">{t('gifts.imageTab.ownFile')}</span>
                                <small className="text-muted">{t('gifts.imageTab.ownFileDescription')}</small>
                            </span>
                        </button>
                    </div>
                    <div className="mb-2 col-md-6 col-lg-4 col-xl-3">
                        <button
                            className={`btn btn__img-source ${activeTab === 1 ? 'active' : ''}`}
                            onClick={setAIActiveTab}
                        >
                            <span className="desc">
                                <span className="title">{t('gifts.imageTab.aiGenerated')}</span>
                                <small className="text-muted">{t('gifts.imageTab.aiGeneratedDescription')}</small>
                            </span>
                        </button>
                    </div>
                    {ENVIRONMENT !== 'prod' ?
                        <div className="mb-2 col-md-6 col-lg-4 col-xl-3">
                            <button
                                className={`btn btn__img-source ${activeTab === 2 ? 'active' : ''}`}
                                onClick={() => setTemplateActiveTab()}
                            >
                            <span className="desc">
                                <span className="title">{t('gifts.imageTab.dailyTemplate')}</span>
                                <small className="text-muted">{t('gifts.imageTab.dailyTemplateDescription')}</small>
                            </span>
                                <span className="img"><img src="/static/img/templateTab.png" alt=""/></span>
                            </button>
                        </div>
                        :
                        null
                    }
                </div>
                <SelectImageBlock/>
                <TitleAndDescriptionBlock
                    descriptionLabel={t('form.label.description')}
                    titleLabel={t('form.label.giftName')}
                />
                {selectedCollection !== publicDropdown.id ? <PropertiesBlock/> : null}
                <RecipientsBlock/>
                {selectedCollection !== publicDropdown.id ? <CollateralBlock/> : null}
                {checkButtonDisabled() ?
                    <div className="row">
                        <div className="col-lg-8">
                            <AlertElement type={'danger'}>
                                <Trans i18nKey={'error.fillAllFields'} components={[<span className="text-danger"/>]}/>
                            </AlertElement>
                        </div>
                    </div>
                    :
                    null
                }
                <div className="row mb-6">
                    <div className="col-lg-3">
                        <ButtonElement
                            className={'w-100'}
                            onClick={createGifts}
                        >{t('button.createGifts')}</ButtonElement>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default Gifts
