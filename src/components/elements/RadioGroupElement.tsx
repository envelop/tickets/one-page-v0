import React, {ReactNode} from 'react'
import {LabelElement, RadioElement} from './index'
import {IDropdownItem} from '../../store/types'

interface propsType {
    errorText?: string
    label?: ReactNode
    list: IDropdownItem[]
    onChange: (value: string) => void
    value: string
}

const RadioGroupElement = (props: propsType) => {
    return <>
        {props.label ? <LabelElement>{props.label}</LabelElement> : null}
        <div className="row gx-4">
            {props.list.map((item) => (
                <div className="col-auto mb-2" key={item.id}>
                    <RadioElement
                        checked={item.id === props.value}
                        onChange={() => {
                            props.onChange(item.id)
                        }}
                        value={item.id}
                        label={item.name}
                    />
                </div>
            ))}
        </div>
        {props.errorText && props.errorText !== '' ?
            <div className="invalid-feedback">{props.errorText}</div>
            :
            null
        }
    </>
}

export default RadioGroupElement
