import React, {ReactNode} from 'react'

interface propsType {
    additionalClass?: string
    disabled?: boolean
    error?: boolean
    errorText?: string
    label?: ReactNode
    onChange: (value: string) => void
    rows?: number
    value: string
}

const TextareaElement = (props: propsType) => {
    const error = props.error || (props.errorText && props.errorText !== '')

    return <>
        {props.label ?
            <label className="form-label">{props.label}</label>
            :
            null
        }
        <textarea
            className={`form-control ${error ? 'is-invalid' : ''} ${props.additionalClass || ''}`}
            rows={props.rows || 3}
            value={props.value}
            onChange={(e) => {
                props.onChange(e.target.value)
            }}
            disabled={props.disabled}
        ></textarea>
        {props.errorText !== '' ?
            <div className="invalid-feedback">{props.errorText}</div>
            :
            null
        }
    </>
}

export default TextareaElement
