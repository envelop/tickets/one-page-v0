import React from 'react'

interface propsType {
    checked: boolean
    label?: string
    onChange: () => void
    readonly?: boolean
    value: string
}

const RadioElement = (props: propsType) => {
    return <>
        <label className="form-check">
            <input
                className="form-check-input"
                type="checkbox"
                value={props.value}
                checked={props.checked}
                onChange={props.onChange}
                readOnly={props.readonly}
            />
            <div className="form-check-radio"></div>
            <div className="form-check-label">{props.label}</div>
        </label>
    </>
}

export default RadioElement
