import React, {useEffect, useState} from 'react'
import {NavLink, useParams} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux'
import {ethers} from 'ethers'
import Tippy from '@tippyjs/react'
import {useTranslation} from 'react-i18next'
import {ButtonElement, CopyToClipboardBlock, DropdownSelector} from '../elements'
import {AppDispatch} from '../../store/store'
import {compactString} from '../../utils/functions'
import {getToken, requestToken} from '../../store/tokensSlice'
import {checkWallet, getWalletAddress, setModalEditTicketPrices, setModalSendTransactions} from '../../store/appSlice'
import {getCoinsObject} from '../../store/coinsSlice'
import {IDropdownItem, IItemOnSale, ITicketPrice} from '../../store/types'
import {CHAINS, CUSTOM_DROPDOWN_ITEM, NULL_ADDRESS} from '../../utils/constants'
import {DiscountIcon, LoadingIcon} from '../icons'
import {buyAssetItem, editAssetItemPrice, getDiscount, requestDiscount} from '../../store/lazyMintingSlice'

interface PropsType {
    eventLink: string
    network?: string
    owner: string
    tokenCard: IItemOnSale
}

const LazyTokenCard = ({eventLink, network, owner, tokenCard}: PropsType) => {
    const {t} = useTranslation()
    const address = tokenCard.nft.asset.contractAddress
    const tokenId = tokenCard.nft.tokenId
    let {showcaseName} = useParams()
    const [selectedPrice, setSelectedPrice] = useState(0)
    let coinsObject = useSelector(getCoinsObject)
    if (network) {
        coinsObject[NULL_ADDRESS] = {
            contract: NULL_ADDRESS,
            token: CHAINS[network].token,
            decimals: CHAINS[network].tokenPrecision,
        }
    }
    const discount = useSelector(getDiscount(`${address}-${tokenId}`))
    const token = useSelector(getToken(address, tokenId))
    const walletAddress = useSelector(getWalletAddress)
    let prices: IDropdownItem[] = []
    for (let i = 0; i < tokenCard.prices.length; i++) {
        let item = tokenCard.prices[i]
        if (coinsObject[item.payWith]) {
            let amount = item.amount
            if (discount) {
                const d = discount > 10000 ? 10000 : discount
                amount = amount.mul((10000 - d)).div(10000)
            }
            prices.push({
                name: `${ethers.utils.formatUnits(amount, coinsObject[item.payWith].decimals)} ${coinsObject[item.payWith].token}`,
                id: i
            })
        }
    }

    const dispatch = useDispatch<AppDispatch>()

    useEffect(() => {
        if (network) {
            dispatch(requestToken({address, tokenId, network}))
            dispatch(requestDiscount({address, tokenId}))
        }
    }, [])

    const buyHandler = () => {
        if (showcaseName) {
            dispatch(checkWallet({
                request: buyAssetItem({
                    item: tokenCard,
                    priceIndex: selectedPrice,
                    eventLink,
                    showcaseName,
                    successText: <p>
                        {t('showcase.goToEvent1')}
                        {' '}
                        <NavLink to={eventLink} onClick={() => dispatch(setModalSendTransactions(null))}>
                            {t('showcase.eventPage')}
                        </NavLink>
                        {' '}
                        {t('showcase.goToEvent2')}
                    </p>,
                }),
                network,
            }))
        }
    }
    const editHandler = () => {
        dispatch(checkWallet({
            request: setModalEditTicketPrices({
                prices: tokenCard.prices,
                setPrices: (value: ITicketPrice[]) => {
                    dispatch(checkWallet({
                        request: editAssetItemPrice({
                            oldPrices: tokenCard.prices,
                            newPrices: value.map(item => ({
                                amount: ethers.utils.parseUnits(item.price.toString(), item.decimals) as ethers.BigNumber,
                                payWith: item.token !== CUSTOM_DROPDOWN_ITEM.id ? item.token : item.customContract,
                            })),
                            assetItem: tokenCard.nft,
                        }),
                        network,
                    }))
                },
            }),
            network,
        }))
    }

    return <div className="mb-4 col-md-6 col-lg-4 col-xl-3">
        <div className="item-card">
            <div className="px-3 py-3">
                <div className="d-flex align-items-center">
                    <div className="me-3">
                        <span>{compactString(address)}</span>
                        <CopyToClipboardBlock text={address}/>
                    </div>
                    <div>
                        <span className="text-muted">ID </span><span>{tokenId.toString()}</span>
                        <CopyToClipboardBlock text={tokenId.toString()}/>
                    </div>
                </div>
            </div>
            <div className="item-card__image">
                {discount ?
                    <Tippy
                        content={t('showcase.discount')}
                        appendTo={document.getElementsByClassName("wrapper")[0]}
                        trigger='mouseenter'
                        interactive={false}
                        arrow={false}
                        maxWidth={512}
                    >
                        <div className="item-card__discount"><DiscountIcon/></div>
                    </Tippy>
                    :
                    null
                }
                <div className="image-container">
                    {token && token.image ?
                        token.image.substring(token.image.lastIndexOf('.') + 1) === 'mp4' ?
                            <video
                                loop={true}
                                autoPlay={true}
                                muted={true}
                            >
                                <source src={token.image} type="video/mp4"/>
                            </video>
                            :
                            <img src={token.image} alt={''}/>

                        :
                        <LoadingIcon/>
                    }
                </div>

            </div>
            <div className="px-3 py-3">
                <div className="mb-3">
                    <DropdownSelector list={prices} currentItem={selectedPrice} setItem={setSelectedPrice}/>
                </div>
                {tokenCard.prices[0] ?
                    owner === walletAddress ?
                        <ButtonElement className={'w-100'} onClick={editHandler}>{t('button.editPrices')}</ButtonElement>
                        :
                        <ButtonElement className={'w-100'} onClick={buyHandler}>{t('button.buy')}</ButtonElement>
                    :
                    null
                }
            </div>
        </div>
    </div>
}

export default LazyTokenCard
