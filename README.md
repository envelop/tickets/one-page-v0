# dApp for creating tickets

[Link](https://app.getpass.is)

## Start script

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Build script

You must set variables in .env file before build (node 18.12.1, npm 8.3.0)

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## Docker Dev Environment with local NGINX

## Docker Dev Environment with local NGINX
```bash
docker run --rm -it -v $PWD:/app node:18.12.1 /bin/bash -c 'cd /app && npm install && chmod -R 777 node_modules'
docker run --rm -it -v $PWD:/app node:18.12.1 /bin/bash -c 'cd /app && npm ci && chmod -R 777 node_modules'
```
```bash
docker run --rm  -it -e PUBLIC_URL=/ -e  REACT_APP_URL=localhost:8080   -v $PWD:/app node:18.12.1 /bin/bash    -c 'cd /app && npm run build'
```

```bash
docker-compose -f docker-compose-local.yaml up
```
http://localhost:8080/
