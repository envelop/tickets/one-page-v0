Map `./public` folder to `./server/public`

In the root folder of project:

Build image:
```docker build -f server/Dockerfile -t tokenviewsdkapp .```

Run container:
```docker run -p 3031:3031 tokenviewsdkapp:latest```