
import re
import os
import traceback
import requests
from datetime import datetime
from dotenv import load_dotenv
load_dotenv()

def format_now():
	return datetime.now().strftime('%Y-%m-%d %H:%M:%S')

url_regex = re.compile('token\/\d*\/0x\w*\/\d*')
remove_old_regexp = re.compile('<meta (name|property)=\"\w*(:)?(image|description|title)\"[^>]*>')

API_ENDPOINT = os.environ.get('API_ENDPOINT', None)
PARAM_UPDATE_SECONDS = int(os.environ.get('PARAM_UPDATE_SECONDS', '86400'))
if API_ENDPOINT is None:
	print('No API_ENDPOINT in .env')

APP_BASE_URL = os.environ.get('APP_BASE_URL', '')

OG_IMAGE_TAGS = [
	'''<meta property="og:url" content="{token_url}">''',
	'''<meta property="og:name" content="{page_name}">''',
	'''<meta property="og:description" content="{page_description}">''',
	'''<meta property="og:image" content="{image_url}">''',
	'''<meta property="twitter:domain" content="{APP_BASE_URL}">''',
	'''<meta property="twitter:url" content="{token_url}">''',
	'''<meta name="twitter:image" content="{image_url}">''',
	'''<link rel="preload" as="image" href="{image_url}" as="image" />''',
	'''<meta name="description" content="{page_description}">'''
]

FETCHED_PARAMS = {
	"updated": None,
	"name": None,
	"description": None,
	"image": None,
}

def fetch_data():
	print('Updating params')
	if API_ENDPOINT is None:
		print('No API_ENDPOINT in .env')
		return

	resp = requests.get(API_ENDPOINT)
	respParsed = resp.json()

	global FETCHED_PARAMS
	FETCHED_PARAMS = {
		"updated": datetime.now(),
		"name": respParsed['name'],
		"description": respParsed['description'],
		"image": respParsed['image'],
	}
	print('Params fetched successfully:', FETCHED_PARAMS)

fetch_data()

def application(env, start_response):

	if FETCHED_PARAMS['name'] is None:
		start_response('500 Internal Server Error', [('Content-Type','text/html')])
		return [b"Cannot fetch params"]

	now = datetime.now()
	if FETCHED_PARAMS['updated'] is not None:
		last_update = now - FETCHED_PARAMS['updated']
		if last_update.total_seconds() > PARAM_UPDATE_SECONDS:
			fetch_data()
	else:
		fetch_data()

	template = None
	template_parts = None
	try:
		f = open('index.html', 'r')
	except FileNotFoundError:
		start_response('500 Internal Server Error', [('Content-Type','text/html')])
		return [b"Cannot open template"]
	else:
		with f:
			template = f.readlines()

	template_parts = remove_old_regexp.sub('', template[0])
	template_parts = template_parts.split('</head>')

	response = []
	url = env['REQUEST_URI']

	token_url = f'{APP_BASE_URL}{url}'
#	image_url = FETCHED_PARAMS['image']
#	page_name = FETCHED_PARAMS['name']
#	page_description = FETCHED_PARAMS['description']
	image_url = '/static/img/preview.jpg'
	page_name = 'NFT tickets. AI-NFT gifts. Web3 accesses. SBT-certificates'
	page_description = 'Create NFT tickets, distribute Web3 gifts, and manage events of any scale with our innovative dApps. Simplify event management and unlock Web3 possibilities'

	response.append(bytes(template_parts[0], 'UTF-8'));
	for item in OG_IMAGE_TAGS:
		formatted = item.format(
			APP_BASE_URL = APP_BASE_URL,
			token_url = token_url,
			image_url = image_url,
			page_name = page_name,
			page_description = page_description
		)
		response.append(bytes(formatted, 'UTF-8'))
	response.append(bytes('</head>', 'UTF-8'));
	response.append(bytes(template_parts[1], 'UTF-8'));

	start_response('200 OK', [('Content-Type','text/html')])
	return response

